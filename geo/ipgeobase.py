# -*- coding: utf-8 -*-
import os
import socket
import struct

from cStringIO import StringIO

from django.contrib.gis.geoip import GeoIPException
from django.contrib.gis.geoip.libgeoip import GEOIP_SETTINGS
from django.core.validators import ipv4_re
from django.conf import settings


cities_format = '64s'
cities_size = struct.calcsize(cities_format)

cidr_format = '2IH2s'
cidr_size = struct.calcsize(cidr_format)


GEOIP_SETTINGS.update({
    'IPGEOBASE_COUNTRY': getattr(settings, 'IPGEOBASE_COUNTRY', 'IPGeoBase.dat'),
    'IPGEOBASE_CITY': getattr(settings, 'IPGEOBASE_CITY', 'IPGeoBaseCity.dat'),
})


def ip2toint(ip):
    return struct.unpack("!I", socket.inet_aton(ip))[0]


class GeoIP(object):
    def __init__(self, path=None, cache=0, country=None, city=None):
        path = path or GEOIP_SETTINGS.get('GEOIP_PATH', None)
        if not path:
            raise GeoIPException('GeoIP path must be provided via parameter or the GEOIP_PATH setting.')
        self._path = path

        country = country or GEOIP_SETTINGS['IPGEOBASE_COUNTRY']
        self._country_filename = os.path.join(path, country)

        city = city or GEOIP_SETTINGS['IPGEOBASE_CITY']
        self._city_filename = os.path.join(path, city)

        self._cache = cache

        self._city_file_buffer = None
        self._country_file_buffer = None

    def __del__(self):
        if self._city_file_buffer:
            self._city_file_buffer.close()

        if self._country_file_buffer:
            self._country_file_buffer.close()

    def convert(self, path=None, country=None, city=None, encoding=None, errors=None):
        cities_numbers = []
        cities_txt = os.path.join(path or self._path, city or 'cities.txt')
        with open(cities_txt, 'r') as t, open(self._city_filename, 'wb') as d:
            for l in t.readlines():
                args = [encoding or 'cp1251']
                if errors:
                    args.append(errors)
                rec = l.decode(*args).rstrip('\n').encode('utf8').split('\t')
                cities_numbers.append(int(rec[0]))
                d.write(struct.pack(
                    cities_format,
                    rec[1]  # city
                ))

        data = []
        countries_txt = os.path.join(path or self._path, country or 'cidr_optim.txt')
        with open(countries_txt, 'r') as t:
            for l in t.readlines():
                rec = l.rstrip('\n').split('\t')
                data.append((
                    int(rec[0]),  # start
                    int(rec[1]),  # end
                    0 if rec[4] == '-' else cities_numbers.index(int(rec[4])) + 1,  # city index
                    rec[3].upper()  # country code
                ))
        data.sort(key=lambda rec: rec[0])
        with open(self._country_filename, 'wb') as d:
            for rec in data:
                d.write(struct.pack(
                    cidr_format,
                    *rec
                ))

    def _get_country(self, ip):
        if not self._country_file_buffer:
            with open(self._country_filename, 'rb') as f:
                self._country_file_buffer = StringIO(f.read())

        self._country_file_buffer.seek(0, 2)
        cnt = self._country_file_buffer.tell() / cidr_size

        left = 0
        right = cnt - 1
        cur = int(right / 2)
        rec = None

        while left < right:
            self._country_file_buffer.seek(cur * cidr_size)
            rec = struct.unpack(cidr_format, self._country_file_buffer.read(cidr_size))
            if rec[0] <= ip and ip <= rec[1]:
                break
            elif rec[0] > ip:
                right = cur - 1
            else:
                left = cur + 1
            cur = int((left + right) / 2)

        if left > right:
            return {
                'country_code': '',
                'city_id': 0,
            }

        return {
            'city_id': rec[2],
            'country_code': rec[3],
        }

    def _get_city(self, cidr):
        city_id = cidr.pop('city_id')
        cidr['region'] = ''
        cidr['city'] = ''
        cidr['country_name'] = ''
        if city_id:
            if not self._city_file_buffer:
                with open(self._city_filename, 'rb') as f:
                    self._city_file_buffer = StringIO(f.read())
            self._city_file_buffer.seek((city_id - 1) * cities_size)
            data = self._city_file_buffer.read(cities_size)
            if data:
                rec = struct.unpack(cities_format, data)
                cidr['city'] = rec[0].decode('utf8').rstrip('\x00')
        return cidr

    def city(self, query):
        if not ipv4_re.match(query):
            query = socket.gethostbyname(query)
        ip = ip2toint(query)
        try:
            return self._get_city(self._get_country(ip))
        except Exception as e:
            raise GeoIPException(e)

    country = city

    def country_code(self, query):
        return self.city(query)['country_code']
