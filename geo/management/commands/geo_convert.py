from django.core.management.base import BaseCommand, CommandError
from geo.ipgeobase import GeoIP


class Command(BaseCommand):
    args = '<path to databases>'
    help = 'Converts .txt ipgeobases to binary representation'

    def handle(self, *args, **options):
        if args:
            GeoIP().convert(args[0])
        else:
            GeoIP().convert()
