#!/usr/bin/env python
from setuptools import setup, find_packages


version = '0.1.0'

setup(
    name='django-rugeoip',
    version=version,
    description="IP geo location",
    long_description=open("README.md").read(),
    classifiers=[
        "Programming Language :: Python",
        'Programming Language :: Python :: 2.7',
        "Framework :: Django",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    keywords='',
    author='adonweb',
    author_email='dev@fitshop.me',
    url='',
    license='BSD',
    packages=find_packages(),
    zip_safe=False,
    install_requires=[
         "django >= 1.5",
         "setuptools",
    ],
)
